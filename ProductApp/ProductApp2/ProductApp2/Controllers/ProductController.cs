﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ProductApp2.Models;

namespace ProductApp2.Controllers
{
    public class ProductController : ApiController
    {
        public IHttpActionResult Get()
        {
            IHttpActionResult ret;
            ProductDB db = new ProductDB();
            if (db.Products.Count() > 0)
            {
                ret = Ok(db.Products);
            }else
            {
                ret = NotFound();
            }
            return ret;

        }
    }


}