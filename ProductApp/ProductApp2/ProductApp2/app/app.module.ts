import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { ProductService } from './product/product.service';
import { ProductListComponent } from './product/product-list.component';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent }  from './app.component';

@NgModule({
    imports: [BrowserModule, HttpModule, AppRoutingModule],
  declarations: [ AppComponent, ProductListComponent ],
  bootstrap: [AppComponent],
  providers: [ProductService]
})
export class AppModule { }
