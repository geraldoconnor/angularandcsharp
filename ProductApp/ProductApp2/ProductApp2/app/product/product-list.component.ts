﻿import {Component} from "@angular/core";
import { OnInit } from "@angular/core";

import {Product} from "./product";
import { ProductService } from "./product.service";

@Component({
    moduleId: module.id,
    templateUrl: "./product-list.component.html"
})

export class ProductListComponent implements OnInit {
    constructor(private productService: ProductService){}

    ngOnInit(): void {
        this.getProducts();
    }

    //public properties
    products: Product[] = [];
    messages: string[] = [];

    private getProducts(): void {
        this.productService.getProducts().subscribe(
            products=> this.products = products,
            errors=> this.handleErrors(errors));
    }

    private handleErrors(errors: any): void {
        for (let msg of errors) {
            this.messages.push(msg);
        }
    }
}

