﻿import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/observable/throw';

import { Product } from "./product";


@Injectable()
export class ProductService {
    private url = "api/product";
    constructor(private http: Http) {
    }
    getProducts(): Observable<Product[]> {
        return this.http.get(this.url).map(this.extractData).catch(this.handleError);
    }
    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }
    private handleError(error: any): Observable<any> {
        let errors: string[] = [];
        switch (error.status) {
            case 404: // not found
                errors.push("No product data is availale");
                break;

            case 500: // Internale Error
                errors.push(error.json().exceptionMessage);
                break;

            default:
                errors.push("Status: " + error.status + "- Error Message" + error.statusText);
                break;

        };
        console.error('An error occured: ', errors);
        return Observable.throw(errors);
    }
}

